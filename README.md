# JGraph

JGraph is a Java library, with which you can create diagrams and graphs. Diagrams can have interactive features.
This is useful, if you want to create diagrams, which can be changed and expanded by a user.

This project is a fork of [JGraph](https://mvnrepository.com/artifact/jgraph/jgraph) written by the University Siegen. It is a mixture between version 1.0 and 2.0.

<br />

## How to add

JGraph can be installed by following steps:

 * open the project folder of your maven project in which you want to install the JGraph library
 * open the `pom.xml` and add the following changes:

**Preconditions**  
The package is located on the package registry in GitLab. In order for Maven to take the correct repository registry, the `pom.xml` must be extended:

```xml
<!-- extend/add repositories -->
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.hochschule-stralsund.de/api/v4/projects/550/packages/maven</url>
  </repository>
  ...
</repositories>

<!-- extend/add distributionManagement -->
<distributionManagement>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.hochschule-stralsund.de/api/v4/projects/550/packages/maven</url>
  </repository>
    
  <snapshotRepository>
    <id>gitlab-maven</id>
    <url>https://gitlab.hochschule-stralsund.de/api/v4/projects/550/packages/maven</url>
  </snapshotRepository>
</distributionManagement>
```

After that the package can be added in the `pom.xml` under the `dependencies`:

```xml
<dependencies>
  <dependency>
    <groupId>de.jgraph.uni-siegen</groupId>
    <artifactId>jgraph</artifactId>
    <version>1.0.1</version>
  </dependency>
  ...
</dependencies>
```

Finally, the package only needs to be installed with `mvn install`.

## System requirements

For using JGraph, please have Java 8 installed on your system.

## How to compile

- open the JGraph project
  - run the command `mvn install`

## Contributors

- University Siegen:
  - specialist group „Didaktik der Informatik und E-Learning“ summer semester 2007
- Hochschule Stralsund – University of Applied Sciences:
  - course group "Softwareprojektorganisation" winter semester 2021
    - Erik der Glückliche - contact [at] lui-studio.net (outsource the source code from main application to a separate Maven repository (create this Git repo))

## License

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

<br>
